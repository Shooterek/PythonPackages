from setuptools import setup, find_packages

setup(
    name='python-package-example',
    version='0.1',
    packages=find_packages(exclude=['tests*']),
    license='MIT',
    description='An example python package',
    install_requires=['numpy', 'math'],
    url='https://gitlab.com/Shooterek/PythonPackages',
    author='Bartek Plokarz',
    author_email='bplokarz@e-science.com'
)
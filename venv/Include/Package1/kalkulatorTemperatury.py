import numpy

def fahrNaKelv(temp):
    '''
    takes a temperature `temp` in fahrenheit and returns it in Kelvin
    '''

    kelvin = 5./9. * (temp - 32.) + 273.15

    return kelvin


def fahrNaCels(temp):
    '''
    takes a temperature `temp` in fahrenheit and returns it in Celsjus
    '''

    celsjusz = 5./9. * (temp - 32.)

    return celsjusz

def celsNaKelv(temp):
    '''
    takes a temperature `temp` in celsjuj and returns it in fahrenheit
    '''

    fahr = 9./5. * (temp + 32.)

    return fahr